package com.ndood.creational.no04_simple_factory.core.impl;

import com.ndood.creational.no04_simple_factory.core.Product;

public class Factory {
	
	public static Product factoryMethod(String args) {
		if ("A".equalsIgnoreCase(args)) {
			return new ConcreteProductA();
		}
		if ("B".equalsIgnoreCase(args)) {
			return new ConcreteProductB();
		}
		return null;
	}

}