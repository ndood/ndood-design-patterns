package com.ndood.creational.no04_simple_factory.sample2;
public abstract class User {
	
	public void sampleOperation() {
		System.out.println("do sample operation...");
	}

	public abstract void differOperation();

}