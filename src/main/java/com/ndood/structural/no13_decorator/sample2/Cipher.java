package com.ndood.structural.no13_decorator.sample2;

public interface Cipher {
	
	public String encrypt(String plainText);

}
