package com.ndood.structural.no13_decorator.core.impl;

import com.ndood.structural.no13_decorator.core.Component;
import com.ndood.structural.no13_decorator.core.Decorator;

public class ConcreteDecoratorB extends Decorator{
	
	public ConcreteDecoratorB(Component component) {
		super(component);
	}

	@Override
	public void operation() {
		super.operation();
		addedBehavior();
	}

	public void addedBehavior() {
		System.out.println("ConcreteDecoratorB.addedBehavior");
	}
}
