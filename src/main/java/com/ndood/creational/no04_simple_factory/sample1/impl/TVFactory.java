package com.ndood.creational.no04_simple_factory.sample1.impl;

import com.ndood.creational.no04_simple_factory.sample1.TV;

public class TVFactory {

	public static TV produceTV(String brand){
		if("Haier".equalsIgnoreCase(brand)){
			return new HaierTV();
		}
		if("Hisense".equalsIgnoreCase(brand)){
			return new HisenseTV();
		}
		return null;
	}

}