package com.ndood.structural.no13_decorator.core;

public interface Component {
	
	public void operation();
	
}
