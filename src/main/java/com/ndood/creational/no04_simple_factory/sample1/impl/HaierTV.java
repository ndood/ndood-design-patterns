package com.ndood.creational.no04_simple_factory.sample1.impl;

import com.ndood.creational.no04_simple_factory.sample1.TV;

public class HaierTV implements TV{

	public void play() {
		System.out.println("Haier TV is playing...");
	}

}