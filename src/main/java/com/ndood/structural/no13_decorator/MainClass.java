package com.ndood.structural.no13_decorator;

import com.ndood.structural.no13_decorator.core.Component;
import com.ndood.structural.no13_decorator.core.Decorator;
import com.ndood.structural.no13_decorator.core.impl.ConcreteComponent;
import com.ndood.structural.no13_decorator.core.impl.ConcreteDecoratorA;
import com.ndood.structural.no13_decorator.core.impl.ConcreteDecoratorB;
import com.ndood.structural.no13_decorator.sample1.Transform;
import com.ndood.structural.no13_decorator.sample1.impl.AirPlane;
import com.ndood.structural.no13_decorator.sample1.impl.Car;
import com.ndood.structural.no13_decorator.sample1.impl.Robot;
import com.ndood.structural.no13_decorator.sample2.Cipher;
import com.ndood.structural.no13_decorator.sample2.CipherDecorator;
import com.ndood.structural.no13_decorator.sample2.impl.AdvancedCipher;
import com.ndood.structural.no13_decorator.sample2.impl.ComplexCipher;
import com.ndood.structural.no13_decorator.sample2.impl.SimpleCipher;

public class MainClass {
	public static void main(String[] args) {
		Component component = new ConcreteComponent();
		Decorator a = new ConcreteDecoratorA(component,1);
		a.operation();
		System.out.println("-----------------");
		Decorator b = new ConcreteDecoratorB(component);
		b.operation();
		
		System.out.println("=================");
		
		Transform transform = new Car();
		Robot robot = new Robot(transform);
		robot.say();
		System.out.println("-----------------");
		AirPlane airPlane = new AirPlane(transform);
		airPlane.fly();
		
		System.out.println("=================");
		
		Cipher cipher = new SimpleCipher();
		CipherDecorator advanced = new AdvancedCipher(cipher);
		System.out.println(advanced.encrypt("111"));
		System.out.println("-----------------");
		CipherDecorator complex = new ComplexCipher(cipher);
		System.out.println(complex.encrypt("111"));
	}
}
