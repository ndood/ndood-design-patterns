package com.ndood.structural.no13_decorator.core.impl;

import com.ndood.structural.no13_decorator.core.Component;

public class ConcreteComponent implements Component{

	public void operation() {
		System.out.println("ConcreteComponent.operation");
	}
}
