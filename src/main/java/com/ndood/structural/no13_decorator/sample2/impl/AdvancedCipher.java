package com.ndood.structural.no13_decorator.sample2.impl;

import com.ndood.structural.no13_decorator.sample2.Cipher;
import com.ndood.structural.no13_decorator.sample2.CipherDecorator;

public class AdvancedCipher extends CipherDecorator{

	public AdvancedCipher(Cipher cipher) {
		super(cipher);
	}

	@Override
	public String encrypt(String plainText) {
		System.out.println("AdvancedCipher.encrypt");
		String encrypt = super.encrypt(plainText);
		return mod(encrypt);
	}

	private String mod(String encrypt) {
		return "mod"+encrypt;
	}
}
