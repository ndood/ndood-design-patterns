package com.ndood.structural.no13_decorator.core;

public abstract class Decorator implements Component{
	
	private Component component;

	public Decorator(Component component) {
		super();
		this.component = component;
	}

	public void operation() {
		component.operation();
		System.out.println("Decorator.operation");
	};
}
