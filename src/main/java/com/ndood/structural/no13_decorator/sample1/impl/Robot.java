package com.ndood.structural.no13_decorator.sample1.impl;

import com.ndood.structural.no13_decorator.sample1.Changer;
import com.ndood.structural.no13_decorator.sample1.Transform;

public class Robot extends Changer{

	public Robot(Transform transform) {
		super(transform);
	}
	
	public void say() {
		super.move();
		System.out.println("Robot.say");
	}
}
