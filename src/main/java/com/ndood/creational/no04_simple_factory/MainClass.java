package com.ndood.creational.no04_simple_factory;

import com.ndood.creational.no04_simple_factory.core.Product;
import com.ndood.creational.no04_simple_factory.core.impl.Factory;
import com.ndood.creational.no04_simple_factory.sample1.TV;
import com.ndood.creational.no04_simple_factory.sample1.impl.TVFactory;
import com.ndood.creational.no04_simple_factory.sample2.User;
import com.ndood.creational.no04_simple_factory.sample2.impl.UserFactory;

public class MainClass {
	
	public static void main(String[] args) {
		Product x = Factory.factoryMethod("A");
		System.out.println(x.getClass().getSimpleName());
		
		System.out.println("=================");
		
		TV xx = TVFactory.produceTV("Haier");
		xx.play();
		
		System.out.println("=================");
		
		User xxx = UserFactory.getUser(3);
		xxx.sampleOperation();
		xxx.differOperation();
	}

}