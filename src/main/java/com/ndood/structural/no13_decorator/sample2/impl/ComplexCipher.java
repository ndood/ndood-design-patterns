package com.ndood.structural.no13_decorator.sample2.impl;

import com.ndood.structural.no13_decorator.sample2.Cipher;
import com.ndood.structural.no13_decorator.sample2.CipherDecorator;

public class ComplexCipher extends CipherDecorator{

	public ComplexCipher(Cipher cipher) {
		super(cipher);
	}

	@Override
	public String encrypt(String plainText) {
		System.out.println("ComplexCipher.encrypt");
		String encrypt = super.encrypt(plainText);
		return reverse(encrypt);
	}

	private String reverse(String encrypt) {
		return "reverse"+encrypt;
	}
}
