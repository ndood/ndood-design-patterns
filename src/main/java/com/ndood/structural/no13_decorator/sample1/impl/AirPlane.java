package com.ndood.structural.no13_decorator.sample1.impl;

import com.ndood.structural.no13_decorator.sample1.Changer;
import com.ndood.structural.no13_decorator.sample1.Transform;

public class AirPlane extends Changer{

	public AirPlane(Transform transform) {
		super(transform);
	}
	
	public void fly() {
		super.move();
		System.out.println("AirPlane.fly");
	}
}
