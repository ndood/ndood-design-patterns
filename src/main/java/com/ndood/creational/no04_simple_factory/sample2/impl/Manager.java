package com.ndood.creational.no04_simple_factory.sample2.impl;

import com.ndood.creational.no04_simple_factory.sample2.User;

public class Manager extends User{

	@Override
	public void differOperation() {
		System.out.println("do with Manager Operation ...");
	}

}