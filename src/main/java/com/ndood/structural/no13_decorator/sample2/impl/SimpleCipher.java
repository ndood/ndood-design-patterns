package com.ndood.structural.no13_decorator.sample2.impl;

import com.ndood.structural.no13_decorator.sample2.Cipher;

public class SimpleCipher implements Cipher{

	public String encrypt(String plainText) {
		System.out.println("SimpleCipher.encrypt");
		return "encrypt"+plainText;
	}
}
