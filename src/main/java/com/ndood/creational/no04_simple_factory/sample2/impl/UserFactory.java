package com.ndood.creational.no04_simple_factory.sample2.impl;

import com.ndood.creational.no04_simple_factory.sample2.User;

public class UserFactory {

	public static User getUser(int permission) {
		if (1 == permission) {
			return new Administrator();
		}
		if (2 == permission) {
			return new Manager();
		}
		if (3 == permission) {
			return new Employee();
		}
		return null;
	}

}