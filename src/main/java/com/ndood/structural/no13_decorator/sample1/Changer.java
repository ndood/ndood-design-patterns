package com.ndood.structural.no13_decorator.sample1;

public abstract class Changer implements Transform{

	private Transform transform;
	
	public Changer(Transform transform) {
		this.transform = transform;
	}

	public void move() {
		transform.move();
		System.out.println("Changer.move");
	}

}
