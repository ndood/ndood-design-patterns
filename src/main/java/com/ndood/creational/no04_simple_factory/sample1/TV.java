package com.ndood.creational.no04_simple_factory.sample1;
public interface TV {
	
	public void play();
	
}