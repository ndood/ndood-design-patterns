package com.ndood.structural.no13_decorator.sample2;

public abstract class CipherDecorator implements Cipher{

	private Cipher cipher;
	
	public CipherDecorator(Cipher cipher) {
		super();
		this.cipher = cipher;
	}

	public String encrypt(String plainText) {
		System.out.println("CipherDecorator.encrypt");
		return cipher.encrypt(plainText);
	}

}
