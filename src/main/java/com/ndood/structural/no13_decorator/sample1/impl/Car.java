package com.ndood.structural.no13_decorator.sample1.impl;

import com.ndood.structural.no13_decorator.sample1.Transform;

public class Car implements Transform{

	public void move() {
		System.out.println("Car.move");
	}

}
