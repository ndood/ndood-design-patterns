package com.ndood.structural.no13_decorator.sample1;

public interface Transform {

	public void move();
	
}
