package com.ndood.structural.no13_decorator.core.impl;

import com.ndood.structural.no13_decorator.core.Component;
import com.ndood.structural.no13_decorator.core.Decorator;

public class ConcreteDecoratorA extends Decorator{
	
	private Integer addedState;

	public ConcreteDecoratorA(Component component, Integer addedState) {
		super(component);
		this.addedState = addedState;
	}

	@Override
	public void operation() {
		super.operation();
		System.out.println("ConcreteDecoratorA.operation");
		System.out.println("addedState:"+addedState);
	}

}
